# pull offcial base image
FROM python:3.7-alpine AS build-python
COPY ./requirements.txt /
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /wheels -r requirements.txt

FROM python:3.7-alpine

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERED 1
ENV DEBUG 0

# install psycopg2
RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev \
    && pip install psycopg2 \
    && apk del build-deps

# install dependencies from wheels
COPY --from=build-python /wheels /wheels
COPY --from=build-python requirements.txt .
RUN pip install --no-cache /wheels/*

# set working directory
WORKDIR /app

# copy project
COPY . .

# collect static files
RUN python manage.py collectstatic --noinput

# add and runs as non-root user
RUN adduser -D myuser
USER myuser

# run gunnicorn
CMD gunicorn hello_django.wsgi:application --bind 0.0.0.0:$PORT
